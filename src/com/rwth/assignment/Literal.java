package com.rwth.assignment;

public class Literal {
    private int node;
    private int value = -1;
    private boolean isBacktrack = false;

    public void setNode(int node) {
        this.node = node;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setBacktrack(boolean backtrack) {
        isBacktrack = backtrack;
    }

    public int getNode() {
        return node;
    }

    public int getValue() {
        return value;
    }

    public boolean isBacktrack() {
        return isBacktrack;
    }

    @Override
    public String toString() {
        return "Literal{" +
                "node=" + node +
                ", value=" + value +
                ", isBacktrack=" + isBacktrack +
                '}';
    }
}