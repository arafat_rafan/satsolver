package com.rwth.assignment;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

class Solver {
    Stack<Literal> literalStack = new Stack<>();
    Literal unknownLiteralUnitClause = null;
    ArrayList<ArrayList<Literal>> nodeClauses;
    int[] searchLiterals = new int[3517];

    public ArrayList<ArrayList<Literal>> parse_dimacs(String filename) throws IOException {

        ArrayList<ArrayList<Literal>> clauses = new ArrayList<ArrayList<Literal>>();
        BufferedReader br = new BufferedReader(new FileReader(filename));

        String line;
        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (line.startsWith("c")) continue;
            if (line.startsWith("p")) continue;
            ArrayList<Literal> clause = new ArrayList<Literal>();
            for (String literal: line.split("\\s+")) {
                Integer lit = new Integer(literal);
                if (lit == 0) break;
                Literal nodeLiteral = new Literal();
                nodeLiteral.setNode(lit);
                clause.add(nodeLiteral);
            }
            if (clause.size() > 0) {
                clauses.add(clause);
            }
        }
        return clauses;
    }
    public void initializeSearchLiterals(){
        for (int i =0 ; i < 3517; i++){
            searchLiterals[i] = -1;
        }
    }
    public static void main(String[] args) throws IOException {
        Solver solver = new Solver();
        solver.initializeSearchLiterals();
        solver.nodeClauses = solver.parse_dimacs(args[0]);
        if(solver.DPLL()){
            System.out.println("SAT");
            System.exit(10);
        }else{
            System.out.println("UNSAT");
            System.exit(20);
        }
    }
    public boolean DPLL(){
        literalStack.clear();
        if(!BCP()){
            return false;
        }
        while (true) {
            if (!decide()){
                return true;
            }
            while (!BCP()){
                if (!backtrack())
                    return false;
            }
        }
    }
    public boolean BCP(){
        Iterator<ArrayList<Literal>> listIterable = nodeClauses.iterator();
        while (listIterable.hasNext()){
            ArrayList<Literal> literals = listIterable.next();
            if(checkSat(literals)){
                continue;
            }
            int value = unknownLiterals(literals);
            if(value == 1){
                setLiteralForSatisfied();
            }else if(value == 0){
                return false;
            }
        }
        return true;
    }
    public int unknownLiterals(ArrayList<Literal> literals){
        int unknownClauses = 0;
        for(int i = 0; i < literals.size() ; i++){
            int value = findValue(literals.get(i).getNode());
            if(value == -1){
                unknownLiteralUnitClause = literals.get(i);
                unknownClauses++;
            }
            if(unknownClauses > 1){
                return unknownClauses;
            }
        }
        return  unknownClauses;
    }
    public void setLiteralForSatisfied(){
        unknownLiteralUnitClause = getLiteral(unknownLiteralUnitClause, "unitclause");
        unknownLiteralUnitClause.setBacktrack(true);
        literalStack.push(unknownLiteralUnitClause);
        searchLiterals[unknownLiteralUnitClause.node] = unknownLiteralUnitClause.value;
        unknownLiteralUnitClause = null;
    }
    public boolean checkSat(ArrayList<Literal> literals){
        for(int i = 0; i < literals.size() ; i++) {
            int value = findValue(literals.get(i).getNode());
            if(value == 1){
                return true;
            }
        }
        return false;
    }
    public int findValue(int node){
        int rootNode = node;
        int value;

        if(node < 0){
            node *=-1;
        }
        value = searchLiterals[node];
        if(value != -1){
            if(rootNode == node){
                return value;
            }else{
                return value == 0 ? 1 : 0;
            }
        }else {
            return -1;
        }
    }
    public Literal getLiteral(Literal literal, String source){
        Literal newLiteral = new Literal();
        int node = literal.getNode();
        int rootNode = node;
        if(node < 0){
            node *=-1;
        }
        newLiteral.setNode(node);
        if(source.equals("unitclause")){
            if(rootNode < 0){
                newLiteral.setValue(0);
            }else {
                newLiteral.setValue(1);
            }
        }else if(source.equals("decide")){
            newLiteral.setValue(0);
        }else if(source.equals("backtrack")){
            newLiteral.setValue(literal.getValue() == 0 ? 1 : 0);
        }
        return newLiteral;
    }
    public boolean decide(){
        boolean assignedValue = false;
        Iterator<ArrayList<Literal>> listIterable = nodeClauses.iterator();
        while (listIterable.hasNext()){
            for (Literal literal: listIterable.next()
            ){
                if(findValue(literal.getNode()) == -1){
                    literal = getLiteral(literal, "decide");
                    literalStack.push(literal);
                    searchLiterals[literal.node] = literal.value;
                    assignedValue = true;
                    break;
                }
            }
            if(assignedValue){
                break;
            }
        }
        return assignedValue;
    }
    public boolean backtrack(){
        while (true){
            if(literalStack.empty()){
                return false;
            }
            Literal literal = literalStack.pop();
            searchLiterals[literal.node] = -1;
            if(!literal.isBacktrack()){
                literal = getLiteral(literal, "backtrack");
                literal.setBacktrack(true);
                literalStack.push(literal);
                searchLiterals[literal.node] = literal.value;
                return true;
            }
        }
    }
    public class Literal {
        private int node;
        private int value = -1;
        private boolean isBacktrack = false;

        public void setNode(int node) {
            this.node = node;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public void setBacktrack(boolean backtrack) {
            isBacktrack = backtrack;
        }

        public int getNode() {
            return node;
        }

        public int getValue() {
            return value;
        }

        public boolean isBacktrack() {
            return isBacktrack;
        }
        /*@Override
        public String toString() {
            return "Literal{" +
                    "node=" + node +
                    ", value=" + value +
                    ", isBacktrack=" + isBacktrack +
                    '}';
        }*/
    }
}